#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#include "constantes.h"
#include "Bravo.h"
#include "fichier.h"


void bravo(SDL_Surface *ecran, int a)
{
    SDL_Surface *imgbravo=NULL, *reset=NULL, *instrucjeu, *instrucediteur;
    SDL_Rect position, positionfull;
    SDL_Event event;

    int continuer=1;

    instrucjeu = IMG_Load("imgpack/instructions_jeu.jpg");
    instrucediteur = IMG_Load("imgpack/instructions.jpg");
    imgbravo = IMG_Load("imgpack/bravo.jpg");
    reset = IMG_Load("imgpack/reset.jpg");

        position.x = LARGEUR_FENETRE / 2 - imgbravo->w / 2 ;
        position.y = HAUTEUR_FENETRE / 2 - imgbravo->h / 2 ;

        positionfull.x = 0;
        positionfull.y = 0;


        if(a==1)
            SDL_BlitSurface(imgbravo,NULL,ecran,&position);
        else if(a==2)
            SDL_BlitSurface(reset,NULL,ecran,&position);
        else if(a==3)
            SDL_BlitSurface(instrucjeu,NULL,ecran,&positionfull);
        else if(a==4)
            SDL_BlitSurface(instrucediteur,NULL,ecran,&positionfull);

    SDL_Flip(ecran);
    while(continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
        case SDL_QUIT:
            exit(0);
        break;

            case SDL_KEYDOWN:
                if(a>2)
                    continuer=0;
                switch(event.key.keysym.sym)
                {
                case SDLK_RETURN:
                    continuer=0;
                break;
                }
            break;
        }
    }

SDL_FreeSurface(imgbravo);
}
