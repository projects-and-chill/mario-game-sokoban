/*
Constante.h
----------------------

Role : Definition des constante pour tout le programme :
*/
#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL_image.h> /* Inclusion du header de SDL_image (adapter le dossier au besoin) */

#include "constantes.h"
#include "jeu.h"
#include "editeur.h"

int main(int argc, char *argv[])
{
    SDL_Surface *ecran= NULL,*menu=NULL;
    SDL_Rect positionMenu;

    SDL_Init(SDL_INIT_VIDEO);
    SDL_WM_SetIcon(IMG_Load("imgpack/caisse.jpg"),NULL);
    ecran = SDL_SetVideoMode(LARGEUR_FENETRE,(HAUTEUR_FENETRE + ACTION_BARRE),32,SDL_HWSURFACE | SDL_DOUBLEBUF);
    SDL_WM_SetCaption("LE MEILLEUR JEUX",NULL);

    FILE *temp = NULL, *fichier=NULL;
    int lignefichier[TAILLE_MAX];

    if(ecran == NULL)
    { fprintf(stderr,"Impossible de charger le mode video : %s\n",SDL_GetError());
    exit(EXIT_FAILURE);
    }

    menu=IMG_Load("imgpack/menu.jpg");
    positionMenu.x = 0;
    positionMenu.y = 0;

      int continuer = 1;
    SDL_Event event;

        while (continuer)
            {
            SDL_WaitEvent(&event);
            switch(event.type)
                {
                case SDL_QUIT:
                    exit(0);
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym)
                    {
                    case SDLK_1: // Demande � jouer
                        bravo(ecran,3);
                        selecteur(ecran,1);
                        continuer = 1;
                        break;
                    case SDLK_2: // Demande l'�diteur de niveaux
                        bravo(ecran,4);
                        selecteur(ecran,2);
                        continuer = 2;
                        break;
                    case SDLK_0: // Demande l'�diteur de niveaux

                            fichier = fopen("niveau.lvl","w+"); // ETAPE 4
                            temp = fopen("reset.lvl","r");
                            while (fgets(lignefichier, TAILLE_MAX, temp) != NULL)
                             {
                                 fprintf(fichier,"%s",lignefichier);

                             }
                            fclose(fichier);
                            fclose(temp);
                            bravo(ecran,2);
                        break;
                    }
                break;
                }

            SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
            SDL_BlitSurface(menu,NULL,ecran,&positionMenu);

            SDL_Flip(ecran);
            }

    SDL_FreeSurface(menu);
    SDL_Quit();
    return EXIT_SUCCESS;
}

