#ifndef DEF_JEU
#define DEF_JEU

void jouer(SDL_Surface* ecran);
void deplacerCaisse(int *premiereCase, int *secondeCase);
void deplacerJoueur(int carte[][NB_BLOC_LARGEUR],SDL_Rect *pos,int direction);
int testcase2(SDL_Event event);
void rejouer(int carte[][NB_BLOC_LARGEUR], SDL_Rect *PositionJoueur);
#endif

