#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#include "constantes.h"
#include "jeu.h"
#include "editeur.h"
#include "selecteur.h"

void selecteur(SDL_Surface *ecran, int a)
{
    SDL_Surface *imgniveau=NULL;
    SDL_Rect position;
    SDL_Event event;

    int continuer=1;

    imgniveau = IMG_Load("imgpack/choixniveaux.jpg");

    while(continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
        case SDL_QUIT:
            exit(0);
        break;

        case SDL_KEYDOWN:
            switch(event.key.keysym.sym)
            {
                case SDLK_ESCAPE:
                    continuer=0;
                    a=0;
                break;
            }
        break;

        case SDL_MOUSEBUTTONDOWN: //Modifie la varaible ChoixNiveau en fonction de position du clic
            if(event.button.button== SDL_BUTTON_LEFT)
            {
                testcase(&continuer,event);
            }
        break;

        }
        SDL_FillRect(ecran,NULL,SDL_MapRGB(ecran->format, 255,255,255));

        position.x = 0;
        position.y = 0;
        SDL_BlitSurface(imgniveau,NULL,ecran,&position);

    // Mise � jour de l'�cran
    SDL_Flip(ecran);
    }

SDL_FreeSurface(imgniveau);

    if(a==1)
        jouer(ecran);
    else if(a==2)
        editeur(ecran);

}

void testcase( int *continuer ,SDL_Event event)
{
    int cases[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR]={0};
    int i=0,j=0,z=0,n=0, coordclick=0;


    for(j=0; j<NB_BLOC_HAUTEUR; j++)
        for(i=0; i<NB_BLOC_LARGEUR; i++)
            {
                cases[j][i] = z;
                z++;
            }
    coordclick = cases[event.button.y / TAILLE_BLOC][event.button.x / TAILLE_BLOC];

    for(n=0; n<5; n++ )
        if( coordclick == (41 + 2*n))
        {
            choixNiveau = n+1;
            *continuer = 0;
        }
        else if( coordclick == (67 + 2*n))
        {
            choixNiveau = n+5;
            *continuer = 0;
        }
         else if( coordclick == (93 + 2*n))
        {
            choixNiveau = n+10;
            *continuer = 0;
        }
        else if( coordclick == (119 + 2*n))
        {
            choixNiveau = n+15;
            *continuer = 0;
        }
}
