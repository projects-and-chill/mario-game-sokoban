#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#include "constantes.h"
#include "fichier.h"
#define N 4

int chargerNiveau(int niveau[][NB_BLOC_LARGEUR])
{

    FILE *fichier=NULL;
    int i=0,j=0;
    char lignefichier[NB_BLOC_LARGEUR * NB_BLOC_HAUTEUR + 2]={0};

    fichier = fopen("niveau.lvl","r");
    if(fichier == NULL)
        return 0;

    for(i=0; i<choixNiveau; i++)
    {
         fgets(lignefichier,TAILLE_MAX,fichier); // On lit le caract�re
    }
    //fgets(lignefichier,NB_BLOC_LARGEUR * NB_BLOC_HAUTEUR + 2,fichier);

    for(i=0; i<NB_BLOC_LARGEUR;i++)
    {
        for(j=0; j<NB_BLOC_HAUTEUR;j++)
        {
            switch(lignefichier[i*NB_BLOC_HAUTEUR + j])
            {
            case '0':
                niveau[j][i] = MUR;
            break;

            case '1':
                niveau[j][i] = MARIO;
            break;

            case '2':
                niveau[j][i] = VIDE;
            break;

            case '3':
                niveau[j][i] = OBJECTIF;
            break;

            case '4':
                niveau[j][i] = CAISSE;
            break;

            case '5':
                niveau[j][i] = CAISSE_OK;
            break;
            }
        }

    }

fclose(fichier);
return 1;
}


int sauvegarderNiveau(int niveau[][NB_BLOC_LARGEUR])
{
    FILE *temp = NULL, *fichier=NULL;

    int lignefichier[TAILLE_MAX];
    int i=0,j=0;

    temp = fopen("temp.lvl","w");
    fichier = fopen("niveau.lvl","r");

    copieravant(); // etape 1

    fseek(temp,0,SEEK_END); // etape 2

    for(i=0; i<NB_BLOC_LARGEUR; i++)
        for(j=0; j<NB_BLOC_HAUTEUR; j++)
            fprintf(temp,"%d",niveau[j][i]);

    fclose(fichier);
    fclose(temp);

    copierfin(); // etape 3
    copiercoller(); //Etape 4

}

  void copieravant()
{

    FILE *fichier=NULL, *temp=NULL;
    int *lireChar=NULL;
    int i=0,t=0;
    char lignefichier[TAILLE_MAX];

    fichier = fopen("niveau.lvl","r");
    temp = fopen("temp.lvl","w+");

    if(fichier == NULL)
        { printf("Copier Avant n a pas marcher !"); exit(0); }


    for(i=0; i<choixNiveau-1; i++)
    {
         fgets(lignefichier,TAILLE_MAX,fichier); // On lit le caract�re
         fprintf(temp,lignefichier);
    }

    fclose(fichier);
    fclose(temp);

    return 1;
}

void copierfin()
{
    FILE *fichier=NULL, *temp=NULL, *test;
    int i=0,t=0;
    char lignefichier[TAILLE_MAX];
    char caractereActuel;

    temp = fopen("temp.lvl","a");
    fichier = fopen("niveau.lvl","r");

    if(fichier == NULL)
        { printf("Copier FIN n a pas marcher !"); exit(0); }

    for(i=0; i<choixNiveau; i++) //Placer le curseur au bon endroit
        fgets(lignefichier,TAILLE_MAX,fichier);

        putc('\n',temp);
     while (fgets(lignefichier, TAILLE_MAX, fichier) != NULL)
     {
         fprintf(temp,"%s",lignefichier);
     }

    fclose(fichier);
    fclose(temp);
}

void copiercoller()
{
    FILE *temp = NULL, *fichier=NULL;
    int lignefichier[TAILLE_MAX];

    fichier = fopen("niveau.lvl","w+"); // ETAPE 4
    temp = fopen("temp.lvl","r");

    while (fgets(lignefichier, TAILLE_MAX, temp) != NULL)
     {
         fprintf(fichier,"%s",lignefichier);

     }
    fclose(fichier);
    fclose(temp);
}

