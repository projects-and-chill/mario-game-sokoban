#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#include "constantes.h"
#include "jeu.h"
#include "fichier.h"
#include "Bravo.h"


void jouer(SDL_Surface* ecran)
{
    SDL_Surface *mario[4]={NULL};
    SDL_Surface *mur=NULL, *caisse=NULL, *caisseok=NULL, *objectif=NULL, *marioActuel = NULL, *barre=NULL;
    //"MarioActuel" est le Mario qui sera Blitter
    SDL_Rect position, positionJoueur, positionBarre; // positionJoueur � des coordonn�e en Cases
    SDL_Event event;

    int continuer=1,objectifsRestants, i=0, j=0;
    int carte[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR]={0};

    barre = IMG_Load("imgpack/barre_jouer.jpg");
    mur = IMG_Load("imgpack/mur.jpg");
    caisse = IMG_Load("imgpack/caisse.jpg");
    caisseok = IMG_Load("imgpack/caisse_ok.jpg");
    objectif = IMG_Load("imgpack/objectif.png");
    mario[BAS] = IMG_Load("imgpack/mario_bas.gif");
    mario[HAUT] = IMG_Load("imgpack/mario_haut.gif");
    mario[GAUCHE] = IMG_Load("imgpack/mario_gauche.gif");
    mario[DROITE] = IMG_Load("imgpack/mario_droite.gif");

    marioActuel = mario[BAS];
//Fonction qui lit dans le fichier de sauvegarde
    if(!chargerNiveau(carte))
        exit(EXIT_FAILURE);

//Le Jeux recupere dans le fichier, la position de mario + met une case vide a la position
    for(i=0; i<NB_BLOC_LARGEUR; i++)
    {
        for(j=0; j<NB_BLOC_HAUTEUR; j++)
        {
            if(carte[j][i]==MARIO)
            {
            positionJoueur.x = i;
            positionJoueur.y = j;
            carte[j][i]=VIDE;
            }
        }
    }
// Activation de la r�p�tition des touches
SDL_EnableKeyRepeat(100, 100);


//Deplacement
    while(continuer)
    {
        SDL_WaitEvent(&event);
    switch(event.type)
           {
           case SDL_QUIT:
                exit(0);
            break;

           case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
               {
                case SDLK_ESCAPE:
                    continuer=0;
                break;
                case SDLK_w:
                    marioActuel=mario[HAUT];
                    deplacerJoueur(carte,&positionJoueur,HAUT);
                    break;
                case SDLK_s:
                    marioActuel=mario[BAS];
                    deplacerJoueur(carte,&positionJoueur,BAS);
                    break;
                case SDLK_d:
                    marioActuel=mario[DROITE];
                    deplacerJoueur(carte,&positionJoueur,DROITE);
                    break;
                case SDLK_a:
                    marioActuel=mario[GAUCHE];
                    deplacerJoueur(carte,&positionJoueur,GAUCHE);
                    break;
                case SDLK_UP:
                    marioActuel=mario[HAUT];
                    deplacerJoueur(carte,&positionJoueur,HAUT);
                    break;
                case SDLK_DOWN:
                    marioActuel=mario[BAS];
                    deplacerJoueur(carte,&positionJoueur,BAS);
                    break;
                case SDLK_RIGHT:
                    marioActuel=mario[DROITE];
                    deplacerJoueur(carte,&positionJoueur,DROITE);
                    break;
                case SDLK_LEFT:
                    marioActuel=mario[GAUCHE];
                    deplacerJoueur(carte,&positionJoueur,GAUCHE);
                    break;
                case SDLK_r:
                    rejouer(carte,&positionJoueur);
                    break;
               }
            break;

            case SDL_MOUSEBUTTONDOWN:
                switch(testcase2(event))
                {
                case 1:
                    selecteur(ecran,1);
                    continuer=0;
                break;

                case 2:
                    rejouer(carte,&positionJoueur);
                break;
                }
            break;

           }
    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));

    positionBarre.x = 0;
    positionBarre.y = (NB_BLOC_HAUTEUR * TAILLE_BLOC);
    SDL_BlitSurface(barre,NULL,ecran,&positionBarre);

    //On re-blitte TOUT � leur positions + on regarde si il reste des objectifs.

    objectifsRestants=0;
    for(i=0; i<NB_BLOC_LARGEUR; i++)
    {
        for(j=0; j<NB_BLOC_HAUTEUR; j++)
        {
        position.x = i * TAILLE_BLOC;
        position.y = j * TAILLE_BLOC;

            switch(carte[j][i])
            {
            case MUR:
                SDL_BlitSurface(mur,NULL,ecran,&position);
                break;
            case CAISSE:
                SDL_BlitSurface(caisse,NULL,ecran,&position);
                break;
            case CAISSE_OK:
                SDL_BlitSurface(caisseok,NULL,ecran,&position);
                break;
            case OBJECTIF:
                SDL_BlitSurface(objectif,NULL,ecran,&position);
                objectifsRestants = 1;
                break;
            }
        }
    }
        //On termine le jeux Si tout les objectif sont rempli
        if(!objectifsRestants)
            {
            bravo(ecran,1);
            choixNiveau++;
            rejouer(carte,&positionJoueur);
            }

        // On place et blitte le joueur � la bonne position
        position.x = positionJoueur.x * TAILLE_BLOC;
        position.y = positionJoueur.y * TAILLE_BLOC;
        SDL_BlitSurface(marioActuel,NULL,ecran,&position);

    SDL_Flip(ecran);
    }

    SDL_EnableKeyRepeat(0, 0);

    SDL_FreeSurface(barre);
    SDL_FreeSurface(mur);
    SDL_FreeSurface(caisse);
    SDL_FreeSurface(caisseok);
    SDL_FreeSurface(objectif);
    for(i=0;i<4;i++)
        SDL_FreeSurface(mario[i]);
}


void deplacerJoueur(int carte[][NB_BLOC_LARGEUR],SDL_Rect *pos,int direction)
{
    switch(direction)
    {
        case HAUT:
            if(pos->y -1 <0)
                break;
            if(carte[pos->y-1][pos->x] == MUR )
                break;
            if((carte[pos->y-1][pos->x] == CAISSE || carte[pos->y-1][pos->x] == CAISSE_OK)
               && (pos->y - 2 < 0 || carte[pos->y-2][pos->x] == MUR || carte[pos->y-2][pos->x] == CAISSE ||
               carte[pos->y-2][pos->x] == CAISSE_OK))
                break;
            deplacerCaisse(&carte[pos->y-1][pos->x], &carte[pos->y-2][pos->x]);
            pos->y--;
        break;

        case BAS:
            if(pos->y + 1 >= NB_BLOC_HAUTEUR)
                break;
            if(carte[pos->y+1][pos->x] == MUR )
                break;
            if((carte[pos->y+1][pos->x] == CAISSE || carte[pos->y+1][pos->x] == CAISSE_OK)
               && (pos->y + 2 >= NB_BLOC_HAUTEUR || carte[pos->y+2][pos->x] == MUR || carte[pos->y+2][pos->x] == CAISSE ||
               carte[pos->y+2][pos->x] == CAISSE_OK))
                break;

            deplacerCaisse(&carte[pos->y+1][pos->x], &carte[pos->y+2][pos->x]);
            pos->y++;
        break;

         case GAUCHE:
            if(pos->x -1 < 0)
                break;
            if(carte[pos-> y][pos-> x - 1] == MUR )
                break;
            if((carte[pos->y][pos->x - 1] == CAISSE || carte[pos->y][pos->x - 1] == CAISSE_OK)
               && (pos->x - 2 < 0 || carte[pos->y][pos->x - 2] == MUR || carte[pos->y][pos->x-2] == CAISSE ||
               carte[pos->y][pos->x-2] == CAISSE_OK))
                break;

            deplacerCaisse(&carte[pos->y][pos->x-1], &carte[pos->y][pos->x-2]);
            pos->x--;
        break;

        case DROITE:
            if(pos->x +1 >= NB_BLOC_LARGEUR)
                break;
            if(carte[pos->y][pos->x +1] == MUR )
                break;
            if((carte[pos->y][pos->x +1] == CAISSE || carte[pos->y][pos->x +1] == CAISSE_OK)
               && (pos->x + 2 >= NB_BLOC_LARGEUR || carte[pos->y][pos->x + 2] == MUR || carte[pos->y][pos->x + 2] == CAISSE ||
               carte[pos->y][pos->x + 2] == CAISSE_OK))
                break;

            deplacerCaisse(&carte[pos->y][pos->x + 1], &carte[pos->y][pos->x + 2]);
            pos->x++;
        break;


    }
}

void deplacerCaisse(int *premiereCase, int *secondeCase)
{
    if(*premiereCase==CAISSE || *premiereCase == CAISSE_OK)
    {
        if(*secondeCase== OBJECTIF)
            *secondeCase = CAISSE_OK;
        else
            *secondeCase= CAISSE;

        if(*premiereCase == CAISSE_OK)
            *premiereCase = OBJECTIF;
        else
            *premiereCase=VIDE;
    }
}

int testcase2(SDL_Event event)
{
    int cases[NB_BLOC_HAUTEUR+1][NB_BLOC_LARGEUR]={0};
    int i=0,j=0,z=0,n=0, coordclick=0;


    for(j=0; j<NB_BLOC_HAUTEUR+1; j++)
        for(i=0; i<NB_BLOC_LARGEUR; i++)
            {
                cases[j][i] = z;
                z++;
            }
    coordclick = cases[event.button.y / TAILLE_BLOC][event.button.x / TAILLE_BLOC];

    if ( coordclick == 165 )
        n=1;
    else if(coordclick == 168 || coordclick == 167 )
        n=2;

    return n;
}

void rejouer(int carte[][NB_BLOC_LARGEUR], SDL_Rect *positionJoueur)
{
    int i,j;
    chargerNiveau(carte);

    for(i=0; i<NB_BLOC_LARGEUR; i++)
        for(j=0; j<NB_BLOC_HAUTEUR; j++)
            if(carte[j][i]==MARIO)
            {
            positionJoueur->x = i;
            positionJoueur->y = j;
            carte[j][i]=VIDE;
            }
}
