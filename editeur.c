#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#include "constantes.h"
#include "editeur.h"
#include "fichier.h"

void editeur(SDL_Surface* ecran)
{
    SDL_Surface *mur=NULL, *caisse=NULL,*caisseok=NULL, *objectif=NULL, *mario = NULL;
    SDL_Surface *actionbarre=NULL, *cadrerouge=NULL, *curseur=NULL;
    SDL_Rect position, positionCurseur;
    SDL_Event event;

    int carte[NB_BLOC_HAUTEUR][NB_BLOC_LARGEUR]={0};
    int continuer=1, objActuel=MUR, i=0, j=0;
    int leftClickPush =0, rightClickPush=0;

    mur = IMG_Load("imgpack/mur.jpg");
    caisse = IMG_Load("imgpack/caisse.jpg");
    objectif = IMG_Load("imgpack/objectif.png");
    mario = IMG_Load("imgpack/mario_bas.gif");
    caisseok = IMG_Load("imgpack/caisse_ok.jpg");
    actionbarre = IMG_Load("imgpack/action_barre.jpg");
    cadrerouge = IMG_Load("imgpack/cadre.png");


    //Placer la sourie au milieu de l'ecran,  avec l'objet par defaut
    curseur = mur;
    SDL_WarpMouse(ecran->w / 2, ecran->h / 2);

    if(!chargerNiveau(carte))
        {exit(EXIT_FAILURE);}

    while(continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
        case SDL_QUIT:
            exit(0);
            break;

        case SDL_MOUSEBUTTONDOWN:
            if(event.button.y < NB_BLOC_HAUTEUR * TAILLE_BLOC )
            {
                if(event.button.button== SDL_BUTTON_LEFT)

                {
                // On met l'objet actuellement choisi (mur, caisse...) � l'endroit du clic
                carte[event.button.y / TAILLE_BLOC][event.button.x / TAILLE_BLOC] =objActuel;
                leftClickPush =1; //puis On active la repetition du click gauche
                }
                else if(event.button.button== SDL_BUTTON_RIGHT)
                {
                    carte[event.button.y / TAILLE_BLOC][event.button.x / TAILLE_BLOC] = VIDE;
                    rightClickPush =1;
                }
            }
        break;

    //Arret de l'enfoncement des clics
        case SDL_MOUSEBUTTONUP:
            if (event.button.button == SDL_BUTTON_LEFT)
                 leftClickPush = 0;
             else if (event.button.button == SDL_BUTTON_RIGHT)
                 rightClickPush = 0;
        break;

        //Poser objet slectionner sur deplacement souris

        case SDL_MOUSEMOTION:
            if(event.motion.y < NB_BLOC_HAUTEUR * TAILLE_BLOC )
                {
                if(leftClickPush)
                    carte[event.motion.y / TAILLE_BLOC][event.motion.x / TAILLE_BLOC] = objActuel;
                else if (rightClickPush)
                    carte[event.motion.y / TAILLE_BLOC][event.motion.x / TAILLE_BLOC] = VIDE;
                }
            positionCurseur.x = event.motion.x;
            positionCurseur.y = event.motion.y;
        break;

        //Appuis sue Touches et Changement d'objet
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym)
            {

                case SDLK_s:
                    sauvegarderNiveau(carte);
                    break;
                case SDLK_c:
                    chargerNiveau(carte);
                    break;
                case SDLK_1:
                    objActuel = MUR;
                    curseur = mur;
                    break;
                case SDLK_2:
                    objActuel = CAISSE;
                    curseur = caisse;
                    break;
                case SDLK_3:
                    objActuel= OBJECTIF;
                    curseur = objectif;
                    break;
                case SDLK_4:
                    objActuel = MARIO;
                    curseur = mario;
                    break;
                case SDLK_5:
                    objActuel = CAISSE_OK;
                    curseur = caisseok;
                    break;
                case SDLK_ESCAPE:
                    continuer=0;
                break;
            }
        break;

        }
        SDL_FillRect(ecran,NULL,SDL_MapRGB(ecran->format, 255,255,255));

       //Placement Barre des taches
        position.x = 0;
        position.y = (NB_BLOC_HAUTEUR * TAILLE_BLOC);
        SDL_BlitSurface(actionbarre,NULL,ecran,&position);

        //Placer le cadre Rouge :
        cadre(objActuel,&position);
        SDL_BlitSurface(cadrerouge,NULL,ecran,&position);

        //Placement des objects
        for(i=0; i<NB_BLOC_LARGEUR; i++)
        {
            for(j=0; j<NB_BLOC_HAUTEUR; j++)
            {
            position.x=i*TAILLE_BLOC;
            position.y=j*TAILLE_BLOC;
                switch(carte[j][i])
                {
                    case MUR:
                        SDL_BlitSurface(mur,NULL,ecran,&position);
                        break;
                    case CAISSE:
                        SDL_BlitSurface(caisse,NULL,ecran,&position);
                        break;
                    case MARIO:
                        SDL_BlitSurface(mario,NULL,ecran,&position);
                        break;
                    case OBJECTIF:
                        SDL_BlitSurface(objectif,NULL,ecran,&position);
                        break;
                    case CAISSE_OK:
                        SDL_BlitSurface(caisseok,NULL,ecran,&position);
                        break;
                }
            }
        }

    //Placement suivi curseur
    SDL_BlitSurface(curseur,NULL,ecran,&positionCurseur);

    // Mise � jour de l'�cran
    SDL_Flip(ecran);
    }

SDL_FreeSurface(cadrerouge);
SDL_FreeSurface(actionbarre);
SDL_FreeSurface(caisseok);
SDL_FreeSurface(mur);
SDL_FreeSurface(caisse);
SDL_FreeSurface(mario);
SDL_FreeSurface(objectif);

}

// Fonction qui affiche l'objet selectionner
void cadre(int objetactuel, SDL_Rect *positionCadre)
{
    if(objetactuel==MUR)
    {
        positionCadre->x = 0*TAILLE_BLOC;
        positionCadre->y = NB_BLOC_HAUTEUR * TAILLE_BLOC;
    }
    else if(objetactuel==CAISSE)
    {
        positionCadre->x = 1*TAILLE_BLOC;
        positionCadre->y = NB_BLOC_HAUTEUR * TAILLE_BLOC;
    }
     else if(objetactuel==OBJECTIF)
    {
        positionCadre->x = 2*TAILLE_BLOC;
        positionCadre->y = NB_BLOC_HAUTEUR * TAILLE_BLOC;
    }
     else if(objetactuel==MARIO)
    {
        positionCadre->x = 3*TAILLE_BLOC;
        positionCadre->y = NB_BLOC_HAUTEUR * TAILLE_BLOC;
    }
     else if(objetactuel==CAISSE_OK)
    {
        positionCadre->x = 4*TAILLE_BLOC;
        positionCadre->y = NB_BLOC_HAUTEUR * TAILLE_BLOC;
    }

}
